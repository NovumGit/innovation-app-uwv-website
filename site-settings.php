<?php

return [
    'config_dir'  => 'novum.uwv',
    'namespace'   => 'NovumUwv',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'uwv.demo.novum.nu',
    'dev_domain'  => 'uwv.demo.novum.nuidev.nl',
    'test_domain' => 'uwv.test.demo.novum.nu',
];
